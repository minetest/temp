package corejava;


public class LoopStatement {

	static Datatypes mytest = new Datatypes();
	
	Character mine = new Character('s');

	
	
	public static void main(String[] args) {
		
		int i=0;
		
		mytest.testmine();
		
		System.out.println("For Loop Starts Here");
		
		for(i=0;i<10;i=i+5)
		
		{
			System.out.println("For Loop i value "+i);
		}
		
		System.out.println("For Loop Ends Here");
		
		System.out.println("While Loop Starts Here");
		
		i=10;
		
		while(i<100)
		{
			System.out.println("While Loop i value "+i);
			i=i+50;
		}
		
		System.out.println("While Loop Ends Here");

		System.out.println("DoWhile Loop Starts Here");
		
		i=100;
		
		do 
		{
			System.out.println("DoWhile Loop i value "+i);
			i=i+50;
		} while(i<200);
		
		System.out.println("DoWhile Loop Ends Here");
		
	}

}
