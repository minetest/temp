package corejava;

public class LocalandGlobalVariables {

	static char var2='M', var3; //Default value for Global variable are Null or Zero
	
	static int var4;
	
	public static void main(String[] args) {
	
		int var1=100;
		
		var2='B';
		
		System.out.println("Local Variable=" +var1);
		
		System.out.println("Global Variable1=" +var2);
		
		System.out.println("Global Variable2=" +var3);
		
		System.out.println("Global Variable2=" +var4);

	}

}
